---
date: 2022-11-26T11:00:59-04:00
description: ""
featured_image: "https://cdn-icons-png.flaticon.com/512/5450/5450588.png"
tags: []
title: "Projets d'avenir"
---

Après mon IUT, il y a plusieurs portes qui s'ouvrent à moi.

Tout d'abord j'ai toujours révé de travailler dans un domaine qui me laisse beaucoup de liberté dans mon travail, donc travailler seul pourrait me plaire.
Mais bien sur, si je veux en arriver là, il faut que je sois dans les meilleurs et travailler encore plus pour intégrer une école d'ingénieur et travailler
dans la cyber sécurité, domaine d'avenir dans les métiers du numérique et de l'informatique.
