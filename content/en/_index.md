---
title: "Louis Hugo Website"

description: "Bienvenue sur mon cv"
cascade:
  featured_image: 'https://www.ionos.fr/digitalguide/fileadmin/DigitalGuide/Teaser/code-editoren-t.jpg'

---

Bonjour je m'appelle Louis Barriac, je suis étudiant à  [l'IUT de Vannes](https://www.iutvannes.fr) bretagnes sud.

Etant en première année, je commence à m'amméliorer dans plusieurs languages comme le JAVA, le Python ou encore le html css et js.

Mais bien sur je ne passe pas tout mon temps libre à coder, je m'intéresse beaucoup d'autres choses comme à l'histoire géoagraphie et d'autres termes comme la géopolitique qui me
permettent d'en savoir plus sur ce qui se passe dans le monde.

Si vous souhaitez me contactez utilisez mon email : barriaclouis@gmail.com.



